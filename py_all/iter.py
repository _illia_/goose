# The list of test Python functions.
# Just change the name of function in the last constructor
# if __name__ to run it from command line.

# Simple list iteration.
def list_iter1():
    mylist = [1, 2, 3]
    for i in mylist:
        print(i)

# List creation and iteration.
def list_iter2():
    mylist = [x*x for x in range(3)]
    for i in mylist:
        print(i)

# Generator example.
def generator1():
    mygenerator = (x*x for x in range(3))
    for i in mygenerator:
        print(i)

# Generator creation.
def create_generator():
    mylist = range(4)
    for i in mylist:
        yield i*i

# Yield example (generator in use).
def invoke_generator():
    mygenerator = create_generator()
    for i in mygenerator:
        print(i)


# Change this constructor to run specific function.
if __name__ == "__main__":
    invoke_generator()
